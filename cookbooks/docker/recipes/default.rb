directory "/etc/docker"

file "/etc/docker/daemon.json" do
  content '{"bip": "172.18.0.1/16"}'
end

execute "Add docker PPA" do
  command <<-EOF
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list
  EOF

  not_if "test -e /etc/apt/sources.list.d/docker.list"
  notifies :update, 'apt_update', :immediately
end

package "docker-ce"
package "docker-ce-cli"
package "containerd.io"

execute "Install docker-compose" do
  command <<-EOF
    curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
  EOF
  not_if "which docker-compose"
end
