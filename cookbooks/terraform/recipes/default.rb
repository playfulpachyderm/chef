execute "Add Hashicorp PPA" do
  command <<-EOF
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
    echo "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
  EOF

  not_if "test -e /etc/apt/sources.list.d/hashicorp.list"
  notifies :update, 'apt_update', :immediately
end

package "terraform"
