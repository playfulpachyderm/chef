# Main database
# -------------

execute "create postgres `www-data` user" do
  user "postgres"
  not_if "psql -c \"\\du\" | grep www-data"
  command <<-EOF
    psql -c "create user \\"www-data\\" with password 'www-data' createdb"
  EOF
end

execute "create database" do
  user "postgres"
  command 'psql -c "create database food with owner \\"www-data\\""'
  notifies :run, "execute[seed database]"
  not_if "psql -lqt | cut -d \\| -f 1 | grep -Eqw \"food\s*$\""
end

execute "seed database" do
  user "www-data"
  command <<-EOF
    psql -d food -f #{node['root_project_dir']}/food/sql_init/schema.sql -a
    psql -d food -f #{node['root_project_dir']}/food/sql_init/triggers.sql -a
    psql -d food -f #{node['root_project_dir']}/food/sql_init/base_foods.sql -a
  EOF
  action :nothing
end



# Test database (not in prod)
# ---------------------------

unless node['environment'] == "production"
  execute "create test database" do
    user "postgres"
    command 'psql -c "create database \\"food-test\\" with owner \\"www-data\\""'
    notifies :run, "execute[seed database test]"
    not_if "psql -lqt | cut -d \\| -f 1 | grep -qw food-test"
  end

  execute "seed database test" do
    user "www-data"
    command <<-EOF
      psql -d food-test -f #{node['root_project_dir']}/food/sql_init/schema.sql -a
      psql -d food-test -f #{node['root_project_dir']}/food/sql_init/triggers.sql -a
      psql -d food-test -f #{node['root_project_dir']}/food/sql_init/base_foods.sql -a
    EOF
    action :nothing
  end
end
