directory "/certs" do
  owner "www-data"
  group "www-data"
  mode "0644"
  action :create
end

# Create SSL certs (if necessary) and link them to `/certs`.
if node['environment'] == "production"
  include_recipe "::use_certbot"
else
  # Only production needs real certs
  include_recipe "::use_snakeoil"
end
