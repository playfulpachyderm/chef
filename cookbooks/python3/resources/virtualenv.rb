resource_name :virtualenv
provides :virtualenv

property :path, String, :name_property => true
property :packages, Array
property :command, String


def command_in_venv(cmd, venv)
  "source #{venv}/bin/activate && #{cmd}"
end


default_action :create

action :create do

  execute "create venv" do
    command "python -m venv #{new_resource.path}"
    not_if "test -e #{new_resource.path}"
  end

  if new_resource.packages.nil?
    return
  end

  new_resource.packages.each do |pkg|
    bash "#{new_resource.path} install #{pkg}" do
      code command_in_venv("pip install #{pkg}", new_resource.path)
      not_if "bash -c '#{command_in_venv("pip show #{pkg}", new_resource.path)}'"
    end
  end
end

action :install do
  new_resource.packages.each do |pkg|
    bash "#{new_resource.path} install #{pkg}" do
      code command_in_venv("pip install #{pkg}", new_resource.path)
      not_if "bash -c '#{command_in_venv("pip show #{pkg}", new_resource.path)}'"
    end
  end
end

action :run_command do
  bash "command" do
    code command_in_venv(new_resource.command, new_resource.path)
  end
end
