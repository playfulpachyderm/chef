resource_name :pip_package
provides :pip_package

property :package, String, :name_property => true
property :version_spec, String
property :venv, String


default_action :install

action :install do
  execute "install" do
    command "pip install #{new_resource.package}#{new_resource.version_spec}"
    not_if "pip show #{new_resource.package}"
  end
end
