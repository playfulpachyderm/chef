package "certbot"
  # options(:ignore_dependencies => "python")
# end

hostname = `hostname`.strip

execute "install SSL cert with certbot" do
  command "certbot --webroot certonly --noninteractive --email playful.pachyderm@gmail.com --agree-tos -d #{hostname}"
  not_if "certbot certificates | grep \"/etc/letsencrypt/live\""
end

link "/certs/#{hostname}.cert" do
  to "/etc/letsencrypt/live/#{hostname}/fullchain.pem"
end

link "/certs/#{hostname}.key" do
  to "/etc/letsencrypt/live/#{hostname}/privkey.pem"
end

# TODO: cron job for `certbot renew`
