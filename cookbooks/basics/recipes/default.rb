apt_update

package "thefuck"
package "dos2unix"
package "expect"
package "jq"
package "curl"

execute "set time zone" do
  only_if 'timedatectl | grep "Time zone" | grep "UTC"'
  command "timedatectl set-timezone America/Toronto"
end

if node['environment'] == "production"
  template "/home/ubuntu/.bashrc" do
    source ".bashrc"
    mode "0644"
  end
  hostname "playfulpachyderm.com"
elsif node['environment'] == "vagrant"
  template "/home/vagrant/.bashrc" do
    source ".bashrc"
    mode "0644"
  end
  hostname "vagrant"  # Do this here since there's nowhere else logical I guess
end
