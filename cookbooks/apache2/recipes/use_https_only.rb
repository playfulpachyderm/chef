# Create site `default-ssl`
vars = {"hostname" => `hostname`.strip}
template "/etc/apache2/sites-available/default-ssl.conf" do
  source "default-ssl.conf"
  variables vars
  notifies :restart, "service[apache2]"
end

# Enable site `default-ssl`
link "/etc/apache2/sites-enabled/default-ssl.conf" do
  to "/etc/apache2/sites-available/default-ssl.conf"
  notifies :restart, "service[apache2]"
end

# Disable HTTP, redirect to HTTPS
vars = {"hostname" => `hostname`.strip}
template "/etc/apache2/sites-available/000-default.conf" do
  source "default.conf"
  variables vars
  notifies :restart, "service[apache2]"
end

# Enable site `000-default`
link "/etc/apache2/sites-enabled/000-default.conf" do
  to "/etc/apache2/sites-available/000-default.conf"
  notifies :restart, "service[apache2]"
end
