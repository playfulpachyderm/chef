package "nginx"

service "nginx"

vars = {"hostname" => `hostname`.strip}

template "/etc/nginx/nginx.conf" do
  source "nginx.conf"
  variables vars
  notifies :restart, "service[nginx]"
end

# Get rid of the default HTTP virtual server:

link "/etc/nginx/sites-enabled/default" do
  action :delete
  notifies :restart, "service[nginx]"
end

file "/etc/nginx/sites-available/default" do
  action :delete
  notifies :restart, "service[nginx]"
end
