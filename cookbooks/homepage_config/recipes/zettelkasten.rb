# pip_requirements_file "#{node['root_project_dir']}/food/requirements.txt"
pip_requirements_file "/app/zettelkasten/requirements.txt"

file "/var/log/the-dump.txt" do
  mode '0755'
  owner 'www-data'
  group 'www-data'
end

link "/etc/systemd/system/zettelkasten.service" do
  to "/app/zettelkasten/zettelkasten.service"
  notifies :restart, "service[zettelkasten]"
end

service "zettelkasten"
