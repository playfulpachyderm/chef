# Install python3
package "python3-pip"
package "python3-venv"

# Make python3 the default python
link "/usr/bin/python" do
  to "/usr/bin/python3"
end

execute "upgrade pip" do
  command "pip3 install --upgrade pip"
  not_if "which pip"
end

pip_package "bs4"
pip_package "pytest"
pip_package "pytest-cov"
pip_package "pylint" do
  version_spec "==2.4.4"
end
