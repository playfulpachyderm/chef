execute "Add Node PPA" do
  command <<-EOF
    curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
    VERSION=node_12.x
    DISTRO="$(lsb_release -s -c)"
    echo "deb https://deb.nodesource.com/$VERSION $DISTRO main" | tee /etc/apt/sources.list.d/nodesource.list
    echo "deb-src https://deb.nodesource.com/$VERSION $DISTRO main" | tee -a /etc/apt/sources.list.d/nodesource.list
  EOF

  not_if "test -e /etc/apt/sources.list.d/nodesource.list"
  notifies :update, 'apt_update', :immediately
end

package "nodejs"

if node['environment'] == "vagrant"
  include_recipe "::vagrant_handle_node_modules"
end
