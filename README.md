# The Chef Repo

The idea is to store all chef stuff in one place.

Different types of projects (i.e. rails app, LAMP stack, django app, etc.) will have different branches.

This project should be added as a submodule to other projects.  Make sure the submodule is tracking the right branch for that project type.

The intention is to use the [ENV Branching Model](https://www.wearefine.com/mingle/env-branching-with-git/), with perhaps some minor modifications.

`master` is the common branch.  Changes that affect all branches should go into `master`.
Each project has its own branch.  Never merge a project branch into master.
Feature branches should be merged into the appropriate feature branch, or `master` if it is generally applicable.  Feature branches can be rebased on top of `master`.


## Generating new base boxes (machine images)

More: https://scotch.io/tutorials/how-to-create-a-vagrant-base-box-from-an-existing-one

Since provisioning is very slow for some stuff, we can cache the results in `generated-boxes`.  A box is generated, provisioned, cleaned (made as small as possible), and packaged as a new "base box" which can then be used instead of `ubuntu/bionic64` in a project's Vagrantfile.

To generate a new box:

1. create (or modify) a Vagrantfile in the `chef` project, and add the necessary "chef_zero" block with the recipes that should be included in the new image.
2. launch it (`vagrant up`)
3. Clean the VM:
```sh
# Delete the apt cache
apt clean

# Dunno what this is for but apparently it helps
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY

# Erase bash history
cat /dev/null > ~/.bash_history && history -c && exit
```
4. Package the image into a new base box: `vagrant package --output <boxname>`
5. Install the box globally: `vagrant box add <boxname> <boxname>`
6. Delete the VM (optional): `vagrant destroy`


## Environments

- "vagrant": Set in Vagrantfile
- "production": set via command-line (`-j production_attributes.json`)
