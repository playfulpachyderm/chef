include_recipe "python3::default"

package "postgresql"
package "postgresql-contrib"

pip_package "psycopg2-binary"

directory "/var/www" do
  owner "www-data"
  group "www-data"
  action :create
end
