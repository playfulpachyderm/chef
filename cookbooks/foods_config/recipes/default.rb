pip_requirements_file "#{node['root_project_dir']}/food/requirements.txt"

# include_recipe "::database"

execute "npm install" do
  cwd "#{node['root_project_dir']}/react"
  command <<-EOF
    mount --bind /home/vagrant/node_modules #{node['root_project_dir']}/react/node_modules
    npm install
  EOF
  only_if "test -z \"$(ls #{node['root_project_dir']}/react/node_modules)\""
end

vars = { :root_project_dir => node['root_project_dir'] }
template "/etc/nginx/sites-available/food-nginx.conf" do
  source "food-nginx.conf"
  variables vars
  notifies :restart, "service[nginx]"
end

# Enable site `food`
link "/etc/nginx/sites-enabled/food-nginx.conf" do
  to "/etc/nginx/sites-available/food-nginx.conf"
  notifies :restart, "service[nginx]"
end

service "postgresql" do
  action :stop
end

directory "/food-data"
directory "/food-data/app"

if node['environment'] != "production"
  directory "/food-data/test"
end
