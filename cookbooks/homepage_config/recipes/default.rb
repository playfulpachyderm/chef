package "fcgiwrap"

# Tiles
# -----

directory "/var/cache/www" do
  owner "www-data"
  group "www-data"
  mode "0755"
  action :create
end

directory "/var/cache/www/tiles" do
  owner "www-data"
  group "www-data"
  mode "0755"
  action :create
end

env = { "PYTHONDONTWRITEBYTECODE" => "true" }
execute "generate tiles" do
  action :nothing
  command "/app/homepage/refresh_tiles.py"
  user "www-data"
  environment env
end

# Clear the cached tiles and regenerate
Dir["/var/cache/www/tiles/*"].each do |path|
  file path do
    action :delete
    not_if "stat -c \"%U %G\" #{path} | grep \"www-data\""
    notifies :run, "execute[generate tiles]"
  end
end

cron "cron tile refreshing" do
  action :create
  minute "*/5"
  command "/app/homepage/refresh_tiles.py"
  environment env
  user "www-data"
end

# DB
# ---------

execute "create database" do
  not_if "sudo -u postgres psql -lqt | cut -d \\| -f 1 | grep -qw homepage"
  command "/app/bootstrap.sh"
end

# Nginx config
# -------------

template "/etc/nginx/sites-available/homepage.conf" do
  source "homepage.conf"
  notifies :restart, "service[nginx]"
end

link "/etc/nginx/sites-enabled/homepage.conf" do
  to "/etc/nginx/sites-available/homepage.conf"
  notifies :restart, "service[nginx]"
end

include_recipe "::zettelkasten"
