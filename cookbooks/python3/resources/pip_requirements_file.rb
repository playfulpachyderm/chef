resource_name :pip_requirements_file
provides :pip_requirements_file

property :filepath, String, :name_property => true

default_action :install

action :install do
  IO.read(new_resource.filepath).split("\n").each do |pkg|
    version = nil
    if pkg.include?("==")
      pkg, version = pkg.split(/==/)

      pip_package pkg do
        version_spec "==#{version}"
      end
    else
      pip_package pkg
    end
  end
end
