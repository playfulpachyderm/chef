package "apache2"

package "libapache2-mod-wsgi-py3"

service "apache2" do
  action :nothing
end

execute "enable python on apache" do
  command <<-EOF
    a2dismod mpm_event
    a2enmod mpm_prefork cgi
  EOF
  notifies :restart, "service[apache2]"
  not_if "apachectl -V | grep -i mpm | grep -i prefork"
end

template "/etc/apache2/apache2.conf" do
  source "apache2.conf"
  notifies :restart, "service[apache2]"
end

execute "enable SSL module on apache" do
  command "a2enmod ssl"
  notifies :restart, "service[apache2]"
  not_if "apachectl -M | grep ssl_module"
end

include_recipe "::use_https_only"
