hostname = `hostname`.strip

link "/certs/#{hostname}.cert" do
  to "/etc/ssl/certs/ssl-cert-snakeoil.pem"
end

link "/certs/#{hostname}.key" do
  to "/etc/ssl/private/ssl-cert-snakeoil.key"
end
